package com.citi.training.trades.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradesNotFoundException;
import com.citi.training.trades.model.Trades;


@Component
@Profile("inmem-dao")
public class InMemTradeDao implements TradesDao {

    private static AtomicInteger idGenerator = new AtomicInteger();

    private Map<Integer, Trades> allTrades = new HashMap<Integer, Trades>();

    @Override
    public Trades create(Trades trades) {
        trades.setId(idGenerator.addAndGet(1));
        allTrades.put(trades.getId(), trades);
        return trades;
    }

    @Override
    public Trades findById(int id) {
        Trades trade = allTrades.get(id);
        if (trade == null) {
            throw new TradesNotFoundException("Trades has not been found");
        }
        return trade;
    }

    @Override
    public List<Trades> findAll() {
        return new ArrayList<Trades>(allTrades.values());
    }

    @Override
    public void deleteById(int id) {
        Trades Trades = allTrades.remove(id);
        if (Trades == null) {
            throw new TradesNotFoundException("Trade could not be deleted");
        }
    }



}
