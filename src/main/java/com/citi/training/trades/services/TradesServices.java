package com.citi.training.trades.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.TradesDao;
import com.citi.training.trades.model.Trades;



@Component
public class TradesServices  {

	@Autowired

	private TradesDao tradeDao;

	public List<Trades> findAll() {
		return tradeDao.findAll();
	}

	public Trades findById(int id) {
		return tradeDao.findById(id);
	}

	public Trades create(Trades trade) {
		return tradeDao.create(trade);
	}

	public void deleteById(int id) {
		tradeDao.deleteById(id);
	}
}

