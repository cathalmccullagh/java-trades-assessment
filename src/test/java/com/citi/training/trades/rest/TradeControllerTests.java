package com.citi.training.trades.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trades.model.Trades;
import com.citi.training.trades.services.TradesServices;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(TradesController.class)
public class TradeControllerTests {

    private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradesServices mockTradesService;

    @Test
    public void findAllTrades_returnsList() throws Exception {
        when(mockTradesService.findAll()).thenReturn(new ArrayList<Trades>());

        MvcResult result = this.mockMvc.perform(get("/trades")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from TradesServices.findAll: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    public void createTrade_returnsCreated() throws Exception {
        Trades testTrade = new Trades(5, "Nokia", 99999.99, 450000);

        this.mockMvc
                .perform(post("/trades").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated()).andReturn();
        logger.info("Result from Create Trade");
    }

    @Test
    public void deleteTrade_returnsOK() throws Exception {
        MvcResult result = this.mockMvc.perform(delete("/trades/5"))
                                       .andExpect(status().isNoContent())
                                       .andReturn();

        logger.info("Result from TradesServices.delete: " +
                    result.getResponse().getContentAsString());
    }

    @Test
    public void getTradeById_returnsOK() throws Exception {
        Trades testTrade = new Trades(1, "AAPL", 23.3, 340574);

        when(mockTradesService.findById(testTrade.getId())).thenReturn(testTrade);

        MvcResult result = this.mockMvc.perform(get("/trades/1"))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from TradesService.getTrades: " +
                    result.getResponse().getContentAsString());
    }

}
