package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;



public class TradesTest {

	private int testId = 53;
    private String testStock = "Samsung";
    private double testPrice = 25000.00;
    private int testVolume = 5300000;
    
    @Test
    public void test_Trades_constructor() {
        Trades testTrade = new Trades(testId, testStock, testPrice, testVolume);;

        assertEquals(testId, testTrade.getId());
        assertEquals(testStock, testTrade.getStock());
        assertEquals(testPrice, testTrade.getPrice(), 0.0001);
        assertEquals(testVolume, testTrade.getVolume());
    }

    @Test
    public void test_Trades_toString() {
        String testString = new Trades(testId, testStock, testPrice, testVolume).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testStock));
        assertTrue(testString.contains(String.valueOf(testPrice)));
        assertTrue(testString.contains((new Integer(testVolume)).toString()));
    }
}
