package com.citi.training.trades.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.model.Trades;



@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradesDaoTests {

    @Autowired
    MysqlTradesDao mysqlTradesDao;

    @Test
    @Transactional
    public void test_createAndFindAll() {
        mysqlTradesDao.create(new Trades(-1, "AAPL", 220.0, 600000));

        assertEquals(1, mysqlTradesDao.findAll().size());
    }

}