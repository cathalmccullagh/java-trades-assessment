package com.citi.training.trades.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.trades.exceptions.TradesNotFoundException;
import com.citi.training.trades.model.Trades;



public class InMemTradeDaoTests {

    private static Logger LOG = LoggerFactory.getLogger(InMemTradeDaoTests.class);

    private int testId = 879;
    private String testStock = "Samsung";
    private double testPrice = 123.99;
    private int testVolume = 879;

    @Test
    public void test_saveAndGetEmployee() {
        Trades testTrades = new Trades(-1, testStock, testPrice, testVolume);
        InMemTradeDao testRepo = new InMemTradeDao();

        testTrades = testRepo.create(testTrades);

        assertTrue(testRepo.findById(testTrades.getId()).equals(testTrades));

    }

    @Test
    public void test_saveAndGetAllEmployees() {
        Trades[] testTradeArray = new Trades[100];
        InMemTradeDao testRepo = new InMemTradeDao();

        for (int i = 0; i < testTradeArray.length; ++i) {
            testTradeArray[i] = new Trades(-1, testStock, testPrice, testVolume);

            testRepo.create(testTradeArray[i]);
        }

        List<Trades> returnedTrades = testRepo.findAll();
        LOG.info("Received [" + returnedTrades.size() + "] Trades from repository");

        for (Trades thisTrade : testTradeArray) {
            assertTrue(returnedTrades.contains(thisTrade));
        }
        LOG.info("Matched [" + testTradeArray.length + "] Trades");
    }

    @Test(expected = TradesNotFoundException.class)
    public void test_deleteTrade() {
        Trades[] testTradeArray = new Trades[100];
        InMemTradeDao testRepo = new InMemTradeDao();

        for (int i = 0; i < testTradeArray.length; ++i) {
            testTradeArray[i] = new Trades(testId + i, testStock, testPrice, testVolume);

            testRepo.create(testTradeArray[i]);
        }
        Trades removedTrade = testTradeArray[5];
        testRepo.deleteById(removedTrade.getId());
        LOG.info("Removed item from Trade array");
        testRepo.findById(removedTrade.getId());
    }

    @Test (expected = TradesNotFoundException.class)
    public void test_deleteNotFoundTrade() {
        InMemTradeDao testRepo = new InMemTradeDao();

        // Add something, so not completely empty
        testRepo.create(new Trades(2, testStock, testPrice, testVolume));
        testRepo.deleteById(99);
    }
}
